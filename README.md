

    Clone the repository.
    cd into the cloned directory
    create the virtual environment
    install the requirement
    cd into project/settings
    rename env.example.py env.py
    Migrate the tables
    Start the server
    Visit the URL "http://127.0.0.1:8000/swagger/"

Users

User List

    URL = http://127.0.0.1:8000/api/users/
    Request Type = GET

User Create 

    URL = http://127.0.0.1:8000/api/users/
    Request Type = POST
    Header
        Content-Type = application/json
    Body 
        email =  "user@example.com"
        last_name = "abc"
        first_name = "cde"
        
        
User View 

    URL = http://127.0.0.1:8000/api/users/{id}
    Request Type = GET
    
Tokens

Token Create

    URL = http://127.0.0.1:8000/api-token-auth/
    Request Type = POST
    Header
        Content-Type = application/json
    Body 
        username =  registered email
        password =  auto generated password
        
        
Token Refresh

    URL = http://127.0.0.1:8000/api-token-refresh/
    Request Type = POST
    Header
        Content-Type = application/json
    Body
        token = generated token
        
Token Verify
    
    URL = http://127.0.0.1:8000/api-token-verify/
    Request Type = POST
    Header
        Content-Type = application/json
    Body
        token = generated token

Courses
        
Course List


    URL = http://127.0.0.1:8000/api/courses/
    Request Type = GET
    Header
        Content-Type = application/json
        Authorization = Bearer {token generated}


Course Create


    URL = http://127.0.0.1:8000/api/courses/
    Request Type = POST
    Header
        Content-Type = application/json
        Authorization = Bearer {token generated}
    Body
        Couerse detail


Course View


    URL = http://127.0.0.1:8000/api/courses/{id}
    Request Type = GET
    Header
        Content-Type = application/json
        Authorization = Bearer {token generated}
