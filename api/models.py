import uuid

from django.db import models


class BaseModel(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class CourseModel(BaseModel):
    name = models.CharField(max_length=256, default=None, null=True, blank=True, unique=True)
    title = models.CharField(max_length=256, default=None, null=True, blank=True)
    subtitle = models.CharField(max_length=256, default=None, null=True, blank=True)
    abstract = models.TextField(default=None, null=True, blank=True)
    overview = models.TextField(default=None, null=True, blank=True)
    intro_video = models.CharField(max_length=256, default=None, null=True, blank=True)

    class Meta:
        ordering = ['name']
        db_table = 'course'

    def __str__(self):
        return "{a}".format(a=self.name)


class FeatureModel(BaseModel):
    course = models.ForeignKey(CourseModel, on_delete=models.CASCADE, default=None, null=True, blank=True,
                               related_name='features')
    name = models.CharField(max_length=64, default=None, null=True, blank=True)

    class Meta:
        unique_together = ['course', 'name']
        ordering = ['course', 'name']
        db_table = 'feature'

    def __str__(self):
        return "{a} {b}".format(a=self.course, b=self.name)


class SkillModel(BaseModel):
    course = models.ForeignKey(CourseModel, on_delete=models.CASCADE, default=None, null=True, blank=True,
                               related_name='skills')
    name = models.CharField(max_length=64, default=None, null=True, blank=True)

    class Meta:
        unique_together = ['course', 'name']
        ordering = ['course', 'name']
        db_table = 'skill'

    def __str__(self):
        return "{a} {b}".format(a=self.course, b=self.name)


class KnowledgePartnerModel(BaseModel):
    course = models.ForeignKey(CourseModel, on_delete=models.CASCADE, default=None, null=True, blank=True,
                               related_name='knowledge_partners')
    name = models.CharField(max_length=64, default=None, null=True, blank=True)
    logo = models.CharField(max_length=64, default=None, null=True, blank=True)

    class Meta:
        unique_together = ['course', 'name']
        ordering = ['course', 'name']
        db_table = 'knowledge_partner'

    def __str__(self):
        return "{a} {b}".format(a=self.course, b=self.name)


class ToolModel(BaseModel):
    course = models.ForeignKey(CourseModel, on_delete=models.CASCADE, default=None, null=True, blank=True,
                               related_name='tools')
    image = models.CharField(max_length=256, default=None, null=True, blank=True)

    class Meta:
        ordering = ['course']
        db_table = 'tool'

    def __str__(self):
        return "{a}".format(a=self.course)


class AnalysisModel(BaseModel):
    course = models.ForeignKey(CourseModel, on_delete=models.CASCADE, default=None, null=True, blank=True,
                               related_name='analyses')
    enrolled = models.IntegerField(default=None, null=True, blank=True)
    placed = models.IntegerField(default=None, null=True, blank=True)
    avg_salary = models.IntegerField(default=None, null=True, blank=True)

    class Meta:
        ordering = ['course', '-created_at']
        db_table = 'analysis'

    def __str__(self):
        return "{a}".format(a=self.course)


class CurriculumModel(BaseModel):
    course = models.ForeignKey(CourseModel, on_delete=models.CASCADE, default=None, null=True, blank=True,
                               related_name='curriculums')
    eligibility = models.CharField(max_length=256, default=None, null=True, blank=True)
    pre_requisite = models.CharField(max_length=256, default=None, null=True, blank=True)

    class Meta:
        ordering = ['course']
        db_table = 'curriculum'

    def __str__(self):
        return "{a}".format(a=self.course)


class ProjectModel(BaseModel):
    course = models.ForeignKey(CourseModel, on_delete=models.CASCADE, default=None, null=True, blank=True,
                               related_name='projects')
    title = models.CharField(max_length=64, default=None, null=True, blank=True)
    description = models.CharField(max_length=256, default=None, null=True, blank=True)

    class Meta:
        unique_together = ['course', 'title']
        ordering = ['course', 'title']
        db_table = 'project'

    def __str__(self):
        return "{a} {b}".format(a=self.course, b=self.title)


class AdvisorModel(BaseModel):
    course = models.ForeignKey(CourseModel, on_delete=models.CASCADE, default=None, null=True, blank=True,
                               related_name='advisors')
    name = models.CharField(max_length=64, default=None, null=True, blank=True)
    linked_in = models.CharField(max_length=64, default=None, null=True, blank=True)
    profile = models.CharField(max_length=256, default=None, null=True, blank=True)
    designation = models.CharField(max_length=64, default=None, null=True, blank=True)
    description = models.CharField(max_length=256, default=None, null=True, blank=True)

    class Meta:
        unique_together = ['course', 'name']
        ordering = ['course', 'name']
        db_table = 'advisor'

    def __str__(self):
        return "{a} {b}".format(a=self.course, b=self.name)


class ReviewModel(BaseModel):
    course = models.ForeignKey(CourseModel, on_delete=models.CASCADE, default=None, null=True, blank=True,
                               related_name='reviews')
    reviewer = models.CharField(max_length=64, default=None, null=True, blank=True)
    review = models.CharField(max_length=256, default=None, null=True, blank=True)

    class Meta:
        ordering = ['course', '-created_at']
        db_table = 'review'

    def __str__(self):
        return "{a} {b}".format(a=self.course, b=self.reviewer)
