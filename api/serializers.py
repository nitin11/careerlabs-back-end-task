from django.contrib.auth.models import User
from rest_framework import serializers

from .models import CourseModel, KnowledgePartnerModel, FeatureModel, SkillModel, ToolModel, AnalysisModel, \
    CurriculumModel, ProjectModel, AdvisorModel, ReviewModel


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'email', 'first_name', 'last_name')


class FeatureSerializer(serializers.ModelSerializer):
    class Meta:
        model = FeatureModel
        exclude = ('course', 'created_at', 'modified_at')


class SkillSerializer(serializers.ModelSerializer):
    class Meta:
        model = SkillModel
        exclude = ('course', 'created_at', 'modified_at')


class KnowledgePartnerSerializer(serializers.ModelSerializer):
    class Meta:
        model = KnowledgePartnerModel
        exclude = ('course', 'created_at', 'modified_at')


class ToolSerializer(serializers.ModelSerializer):
    class Meta:
        model = ToolModel
        exclude = ('course', 'created_at', 'modified_at')


class AnalysisSerializer(serializers.ModelSerializer):
    class Meta:
        model = AnalysisModel
        exclude = ('course', 'created_at', 'modified_at')


class CurriculumSerializer(serializers.ModelSerializer):
    class Meta:
        model = CurriculumModel
        exclude = ('course', 'created_at', 'modified_at')


class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectModel
        exclude = ('course', 'created_at', 'modified_at')


class AdvisorSerializer(serializers.ModelSerializer):
    class Meta:
        model = AdvisorModel
        exclude = ('course', 'created_at', 'modified_at')


class ReviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReviewModel
        exclude = ('course', 'created_at', 'modified_at')


class CourseSerializer(serializers.ModelSerializer):
    knowledge_partners = KnowledgePartnerSerializer(many=True)
    features = FeatureSerializer(many=True)
    skills = SkillSerializer(many=True)
    tools = ToolSerializer(many=True)
    analyses = AnalysisSerializer(many=True)
    curriculums = CurriculumSerializer(many=True)
    projects = ProjectSerializer(many=True)
    advisors = AdvisorSerializer(many=True)
    reviews = ReviewSerializer(many=True)

    class Meta:
        model = CourseModel
        exclude = ('created_at', 'modified_at')
