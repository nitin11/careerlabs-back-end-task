from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.test import APITestCase, APIClient


class AuthTests(APITestCase):
    data = {"username": "testuser", "password": "testuser"}

    def test_Auth(self):
        response = self.client.post('/api/users/', self.data, format='json')
        self.assertEqual(response.status_code)
        self.assertEqual(response.data, {"message": "User created"})
        response = self.client.post('/api/users/', self.data, format='json')
        self.assertEqual(response.status_code)
        self.assertEqual(response.data, {"message": "User exists"})


class CourseTests(APITestCase):
    data = {
            "title": "Test course",
            "subtitle": "123",
            "abstract": "456",
            "overview": "789",
            "introVideo": "234234",
            "curriculums": {
                "eligibility": "sdsdf",
                "pre_requisite": "stsdfsdfsring"
            },
            "features": [
                {
                    "name": "sdsd"
                }
            ],
            "skills": [
                {
                    "name": "sdsd"
                }
            ],
            "tools": [
                {
                    "image": "string"
                }
            ],
       }

    def test_authentication(self):
        new_client = APIClient()
        response = new_client.post('/api/courses/', self.data, format='json')
        self.assertEqual(response.status_code)
