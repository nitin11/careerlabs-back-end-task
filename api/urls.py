from django.urls import path, include
from rest_framework import routers

from .views import *

router = routers.SimpleRouter()
router.register(r'courses', CourseViewSet, basename="courses")
router.register(r'users', UserViewSet, basename="users")

urlpatterns = [
    path('', include(router.urls)),
]
