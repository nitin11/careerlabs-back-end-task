import uuid

from django.contrib.auth.hashers import make_password
from rest_framework import permissions
from rest_framework import status, viewsets
from rest_framework.response import Response

from .serializers import *


class CourseViewSet(viewsets.ModelViewSet):
    serializer_class = CourseSerializer
    permission_classes = (permissions.IsAuthenticated,)
    http_method_names = ['get', 'post', 'delete']

    def get_queryset(self):
        queryset = CourseModel.objects.all()
        return queryset

    def create(self, request):
        response = dict()
        response_data = dict()
        response["data"] = response_data

        data = request.data
        features = data.pop('features', [])
        skills = data.pop('skills', [])
        knowledge_partners = data.pop('knowledge_partners', [])
        tools = data.pop('tools', [])
        analyses = data.pop('analyses', [])
        curriculums = data.pop('curriculums', [])
        projects = data.pop('projects', [])
        advisors = data.pop('advisors', [])
        reviews = data.pop('reviews', [])

        course = CourseModel.objects.create(**data)
        course_serializer = CourseSerializer(course).data

        for feature in features:
            FeatureModel(name=feature.get("name", None), course=course).save()

        for skill in skills:
            SkillModel(name=skill.get("name", None), course=course).save()

        for kp in knowledge_partners:
            KnowledgePartnerModel(name=kp.get("name", None), logo=kp.get("logo", None), course=course).save()

        for tool in tools:
            ToolModel(image=tool.get("image", None), course=course).save()

        for analysis in analyses:
            AnalysisModel(enrolled=analysis.get("enrolled", None), placed=analysis.get("placed", None),
                          avg_salary=analysis.get("avg_salary", None), course=course).save()

        for curriculum in curriculums:
            CurriculumModel(eligibility=curriculum.get("eligibility", None),
                            pre_requisite=curriculum.get("pre_requisite", None),
                            course=course).save()

        for project in projects:
            ProjectModel(title=project.get("title", None), description=project.get("description", None),
                         course=course).save()

        for advisor in advisors:
            AdvisorModel(name=advisor.get("name", None), linked_in=advisor.get("linked_in", None),
                         profile=advisor.get("profile", None), designation=advisor.get("designation", None),
                         description=advisor.get("description", None), course=course).save()

        for rev in reviews:
            ReviewModel(reviewer=rev.get("reviewer", None), review=rev.get("review", None), course=course).save()

        response_data["course"] = course_serializer
        return Response(data=response_data, status=status.HTTP_201_CREATED)

    def retrieve(self, request, pk):
        response = dict()
        response_data = dict()
        response["data"] = response_data

        course = CourseSerializer(CourseModel.objects.get(pk=pk)).data

        response_data["course"] = course
        return Response(data=response_data, status=status.HTTP_200_OK)

    def destroy(self, request, pk):
        course = CourseModel.objects.get(pk=pk)
        course.delete()
        return Response(status=status.HTTP_200_OK)


class UserViewSet(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    permission_classes = (permissions.AllowAny,)
    http_method_names = ['get', 'post']

    def get_queryset(self):
        queryset = User.objects.all()
        return queryset

    def create(self, request):
        response = dict()
        response_data = dict()
        response["data"] = response_data

        data = request.data
        auto_generated_password = str(uuid.uuid4()).replace('-', '')[:10]
        data["password"] = make_password(auto_generated_password)
        data["username"] = data["email"]
        user = UserSerializer(User.objects.create(**data)).data

        user["password"] = auto_generated_password
        response_data["user"] = user
        return Response(data=response_data, status=status.HTTP_201_CREATED)

    def retrieve(self, request, pk):
        response = dict()
        response_data = dict()
        response["data"] = response_data

        user = UserSerializer(User.objects.get(pk=pk)).data

        response_data["user"] = user
        return Response(data=response_data, status=status.HTTP_200_OK)
